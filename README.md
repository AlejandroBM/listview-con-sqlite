# Prueba básica de ListView/SQLite #

Es una prueba básica de ListView/SQLite para ANDROID, solo sirve como ejemplo para siguientes desarrollos.

### What is this repository for? ###

* Material de apoyo
* Android 4.4
* Version 1.0
* [Learn ListView](http://developer.android.com/reference/android/widget/ListView.html)
* [Learn SQLite](http://developer.android.com/reference/android/database/sqlite/package-summary.html)

### Who do I talk to? ###

* Alejandro B.M.
* curtiscatrix@gmail.com or curtisob@hotmail.es
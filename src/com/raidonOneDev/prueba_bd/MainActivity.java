package com.raidonOneDev.prueba_bd;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;


public class MainActivity extends ActionBarActivity {
	
	private ListView listaUsuarios;
	private List<String> datos;

	Menu_Agregar menu_agregarDatos = new Menu_Agregar(MainActivity.this, this);
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    
    public void menuAgregar(View view){
    	menu_agregarDatos.muestraMenuAgregar();
    }
    
    /**
     * Se deja a medias, por que todav�a falta �_�
     */
    
    private Handler llenaListView = new Handler(){
    	public void handleMessage(Message g){
    		listaUsuarios = (ListView) findViewById(R.id.lv_usuarios);
    		
    		datos.add("Hola");
    		
    	}
    	
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

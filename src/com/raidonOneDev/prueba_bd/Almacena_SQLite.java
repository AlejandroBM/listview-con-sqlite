package com.raidonOneDev.prueba_bd;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class Almacena_SQLite extends SQLiteOpenHelper{
	
	// ***** VARIABLES ESTATICAS PARA MI ENTRADA DE DATOS EN SQLITE
	private static final String NOMBRE_TABLE = "datos_generales";
	
	private static final String ID         = "id";
	private static final String NOMBRE     = "nombre";
	private static final String APELLIDO_P = "apellido_P";
	private static final String APELLIDO_M = "apellido_M";
	private static final String EDAD       = "edad";
	private static final String CARRERA    = "carrera";
	private static final String IMAGEN     = "imagen";
	
	//***** DATOS EXTRA
	private Cursor cursor;
	private String nombre;
	
	public Almacena_SQLite(Context context, String name, CursorFactory factory,
			int version) {
		super(context, name, factory, version);
		// TODO Auto-generated constructor stub
	}

	/*
	 * Funcion encagada de crear la base de datos.
	 * (non-Javadoc)
	 * @see android.database.sqlite.SQLiteOpenHelper#onCreate(android.database.sqlite.SQLiteDatabase)
	 */
	@Override
	public void onCreate(SQLiteDatabase db) {

		//Se crea la estructura de la tabla NOMBRE_TABLE(datos generales) con el siguiente QUERY
		db.execSQL("CREATE TABLE "+ NOMBRE_TABLE + " (" +
				    ID         + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
				    NOMBRE     + " TEXT NOT NULL," +
				    APELLIDO_P + " TEXT NOT NULL," +
				    APELLIDO_M + " TEXT NOT NULL, " +
				    EDAD       + " INTEGER NOT NULL, " +
				    CARRERA    + " CHAR(40) NOT NULL," +
				    IMAGEN     + " BLOB)");
	}
	
	/**
	 * Fenci�n que guarda mis dato dentro de la tabla NOMBRE_TABLE(datos_generales)
	 * @param nombre
	 * @param apellidoP
	 * @param apellidoM
	 * @param edad
	 * @param carrera
	 * @param blob
	 */
	public void guardarDatos (String nombre, String apellidoP, String apellidoM, int edad, String carrera){//, byte[] blob
		
		SQLiteDatabase db = getWritableDatabase();
		
		//Se genera un INSERT con el siguiente QUERY para la tabla NOMBRE_TABLE(datos_generales)
		db.execSQL("INSERT INTO " + NOMBRE_TABLE + 
												   "(" +NOMBRE + "," +
												   	 APELLIDO_P + "," +
													 APELLIDO_M + "," + 
												     EDAD + "," +
													 CARRERA + ")" +
				                 " VALUES ('" +
												   	 nombre + "','" +
				                 					 apellidoP + "','" +
												   	 apellidoM + "'," +
				                 					 edad + ",'" +
												   	 carrera + "');");
		
		/*
		 * Para Blob
		 * 
		 * db.execSQL("INSERT INTO " + NOMBRE_TABLE + 
												   "(" +NOMBRE + "," +
												   	 APELLIDO_P + "," +
													 APELLIDO_M + "," + 
												     EDAD + "," +
													 CARRERA + "," +
												   	 IMAGEN + ")" +
				                 " VALUES (" +
												   	 nombre + "," +
				                 					 apellidoP + "," +
												   	 apellidoM + "," +
				                 					 edad + "," +
												   	 carrera + "," +
				                 					 blob + ")");
		 */
	}
	
	public String consulta( String sql){
		SQLiteDatabase db = getWritableDatabase();;
		cursor = db.rawQuery(sql, null);
		
		if(cursor.moveToFirst()){
			nombre = String.valueOf(cursor.getInt(0));
		}
		
		return nombre;
		
	}
	/*
	 * Se elimina la tabla si al instalarse nuevamente el apk, existiera una tabla con el mismo nombre.
	 * (non-Javadoc)
	 * @see android.database.sqlite.SQLiteOpenHelper#onUpgrade(android.database.sqlite.SQLiteDatabase, int, int)
	 */
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		
		db.execSQL("DROP TABLE IF EXISTS " + NOMBRE_TABLE);
		onCreate(db);
		
	}

}

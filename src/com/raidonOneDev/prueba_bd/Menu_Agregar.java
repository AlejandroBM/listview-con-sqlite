package com.raidonOneDev.prueba_bd;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class Menu_Agregar{
	
	private Context contexto;
	private Activity actividad;
	
	private Almacena_SQLite objSQL = new Almacena_SQLite(contexto, null, null, 1);


	
	private String[] carreras = new String[]{"Selecciona la carrera de interes..",
											 "Ing. en Sistemas" , 
											 "Lic. en Mercadotecn�a" , 
											 "Lic. en Psicologia" , 
											 "Lic. en Pedagogia", 
											 "Ing. en Mecatronica",
											 "Lic. en Derecho",
											 "Lic. en Informatica"};
	
	private EditText et_nombre, 
					 et_apellidoP, 
					 et_apellidoM, 
					 et_Edad;
	
	
	private String addNombre,
				   addApellidoP,
				   addApellidoM,
				   addCarrera;
	
	private int addEdad;
	
	
	public Menu_Agregar(Context contexto){
		this.contexto = contexto;
	}
	
	public Menu_Agregar(Context contexto, Activity actividad){
		
		this.contexto = contexto;
		this.actividad = actividad;
		
	}
	
	public void muestraMenuAgregar(){
		
		final Dialog menu = new Dialog(contexto);
		
		
		menu.requestWindowFeature(Window.FEATURE_NO_TITLE);
		menu.setContentView(R.layout.captura_datos);
		
		final Button aceptar = (Button) menu.findViewById(R.id.bt_aceptar);
		final Spinner carreras_spinner = (Spinner) menu.findViewById(R.id.spinner_carreras);
			List<String> list_carreras = new ArrayList<String>();
			for(int i=0; i < carreras.length; i++){
				list_carreras.add(carreras[i]);
			}
		
		//Se agrega codigo para SPINNER, el cual contiene las carreras para la selecci�n del usuario
		ArrayAdapter<String> carrerasAdapter = new ArrayAdapter<String> (contexto, android.R.layout.simple_spinner_item, list_carreras);
		carrerasAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		carreras_spinner.setAdapter(carrerasAdapter);
		carreras_spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				addCarrera = parent.getItemAtPosition(position).toString();
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				
			}
		});
		
		aceptar.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String query = "SELECT * FROM datos_generales;";
				//---
				objSQL.getWritableDatabase();
				
				System.out.println("Consulta de base de datos : " + objSQL.consulta(query));
				//---
				
				et_nombre    = (EditText) menu.findViewById(R.id.et_nombre);
				et_apellidoP = (EditText) menu.findViewById(R.id.et_apellidoP);
				et_apellidoM = (EditText) menu.findViewById(R.id.et_apellidoM);
				et_Edad      = (EditText) menu.findViewById(R.id.et_aedad);
				
				addNombre    = et_nombre.getText().toString();
				addApellidoP = et_apellidoP.getText().toString();
				addApellidoM = et_apellidoM.getText().toString();
				addEdad      = Integer.parseInt(et_Edad.getText().toString());
				//addAcarrera adquiere su valor desde muestraMenuAgregar
				
				objSQL.guardarDatos(addNombre, addApellidoP, addApellidoM, addEdad, addCarrera);
				
				objSQL.close();
				
				
				menu.dismiss();
			}
			
		});
		menu.show();
	}
	

}

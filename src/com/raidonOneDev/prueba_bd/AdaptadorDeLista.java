package com.raidonOneDev.prueba_bd;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class AdaptadorDeLista extends BaseAdapter{

	private Activity actividad;
	private List<String> items;
	
	
	public AdaptadorDeLista(){
		super();
	}
	
	public AdaptadorDeLista(Activity actividad, List<String> items){
		this.actividad = actividad;
		this.items = items;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return items.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		View vista = convertView;
		
		if(convertView == null){
			LayoutInflater inflater = (LayoutInflater) actividad.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			vista = inflater.inflate(R.layout.item_listview	, null);
		}
		 	 
		
		TextView numero  = (TextView) vista.findViewById(R.id.tv_numeroItem);
		TextView nombre  = (TextView) vista.findViewById(R.id.tv_campoNombre);
		TextView edad    = (TextView) vista.findViewById(R.id.tv_campoEdad);
		TextView carrera = (TextView) vista.findViewById(R.id.tv_campoCarrera);
		
		numero.setText(String.valueOf(++position));
		nombre.setText(items.get(1));
		edad.setText(items.get(2));
		carrera.setText(items.get(3));
		
		return vista;
	}

}
